﻿using eCargoTest.Impl;
using eCargoTest.Interface;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCargoTest.UnitTest
{
    [TestClass]
    public abstract class BaseTest
    {
        protected IUnityContainer _container = new UnityContainer();

        [TestInitialize]
        public virtual void Setup()
        {
            _container = new UnityContainer();
            _container.RegisterType(typeof(INodeDescriber), typeof(NodeDescriber));
            _container.RegisterType(typeof(INodeTransformer), typeof(NodeTransformer));
            _container.RegisterType(typeof(INodeWriter), typeof(NodeWriter));

            Console.WriteLine("Setup executed.");
            //begin transaction
        }

        [TestCleanup]
        public virtual void Cleanup()
        {
            Console.WriteLine("Cleanup executed.");
            //rollback transaction
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using eCargoTest.Interface;
using eCargoTest.Impl;
using eCargoTest.Model;
using Microsoft.Practices.Unity;
using System.IO;

namespace eCargoTest.UnitTest
{
    [TestClass]
    public class TestNodeDescriber : BaseTest
    {
        private const string HardOutput = 
@"// new SingleChildNode(""root"",
//     new TwoChildrenNode(""child1"",
//         new NoChildrenNode(""leaf1""),
//         new SingleChildNode(""child2"",
//             new NoChildrenNode(""leaf2""))))";

        [TestMethod]
        public void TestHardDescribe()
        {
            var describer = _container.Resolve<INodeDescriber>();

            var testData = new SingleChildNode("root",
                new TwoChildrenNode("child1",
                    new NoChildrenNode("leaf1"),
                    new SingleChildNode("child2",
                        new NoChildrenNode("leaf2"))));
            var result = describer.Describe(testData);

            Assert.AreEqual(HardOutput, result);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using eCargoTest.Interface;
using eCargoTest.Impl;
using eCargoTest.Model;
using Microsoft.Practices.Unity;
using System.IO;
using System.Threading.Tasks;

namespace eCargoTest.UnitTest
{
    [TestClass]
    public class TestNodeWriter : BaseTest
    {
        private const string FilePath = @"C:\eCargoTestResult\node.txt";

        private const string HardOutput =
@"// new SingleChildNode(""root"",
//     new TwoChildrenNode(""child1"",
//         new NoChildrenNode(""leaf1""),
//         new SingleChildNode(""child2"",
//             new NoChildrenNode(""leaf2""))))";

        [TestMethod]
        public void TestDescribeThenWriteToFile_NotEqual()
        {
            Task.Run(async () =>
            {
                var writer = _container.Resolve<INodeWriter>();
                string filePath = @"C:\eCargoTestResult\node.txt";

                var testData = new SingleChildNode("root",
                    new TwoChildrenNode("child1",
                        new NoChildrenNode("leaf1"),
                        new SingleChildNode("child2",
                            new NoChildrenNode("leaf2"))));
                await writer.WriteToFileAsync(testData, filePath);
                var result = File.ReadAllText(filePath);

                Assert.AreNotEqual("something else", result);

            }).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void TestDescribeThenWriteToFile_Equal()
        {
            Task.Run(async () =>
            {
                var writer = _container.Resolve<INodeWriter>();
                string filePath = @"C:\eCargoTestResult\node.txt";

                var testData = new SingleChildNode("root",
                    new TwoChildrenNode("child1",
                        new NoChildrenNode("leaf1"),
                        new SingleChildNode("child2",
                            new NoChildrenNode("leaf2"))));
                await writer.WriteToFileAsync(testData, filePath);
                var result = File.ReadAllText(filePath);

                Assert.AreEqual(string.Compare(HardOutput, result), 0);

            }).GetAwaiter().GetResult();
        }

        [TestCleanup]
        public override void Cleanup()
        {
            base.Cleanup();
            if (File.Exists(FilePath))
                File.Delete(FilePath);
        }
    }
}

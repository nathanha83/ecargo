﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using eCargoTest.Interface;
using eCargoTest.Impl;
using eCargoTest.Model;
using Microsoft.Practices.Unity;
using System.IO;

namespace eCargoTest.UnitTest
{
    [TestClass]
    public class TestNodeTransformer : BaseTest
    {
        private const string HardOutput =
@"// new SingleChildNode(""root"",
//     new TwoChildrenNode(""child1"",
//         new NoChildrenNode(""leaf1""),
//         new SingleChildNode(""child2"",
//             new NoChildrenNode(""leaf2""))))";

        [TestMethod]
        public void TestHardTransform()
        {
            var transformer = _container.Resolve<INodeTransformer>();

            var testData = new ManyChildrenNode("root",
                new ManyChildrenNode("child1",
                    new ManyChildrenNode("leaf1"),
                    new ManyChildrenNode("child2",
                        new ManyChildrenNode("leaf2"))));

            var result = transformer.Transform(testData);

            var describer = _container.Resolve<INodeDescriber>();

            string output = describer.Describe(result);

            Assert.AreEqual(HardOutput, output);

        }

        [TestMethod]
        public void TestTransformMultipleChildrenNodeToSingleChildNode_Success()
        {
            var transformer = _container.Resolve<INodeTransformer>();

            var testData = new ManyChildrenNode("root",
                new ManyChildrenNode("child1"));

            var result = transformer.Transform(testData);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(SingleChildNode));
        }

        [TestMethod]
        public void TestTransformMultipleChildrenNodeToTwoChildrenNode_Success()
        {
            var transformer = _container.Resolve<INodeTransformer>();

            var testData =
                new ManyChildrenNode("child1",
                    new ManyChildrenNode("leaf1"),
                    new ManyChildrenNode("child2"));

            var result = transformer.Transform(testData);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(TwoChildrenNode));
        }

        [TestMethod]
        public void TestTransformMultipleChildrenNodeToNoChildrenNode_Success()
        {
            var transformer = _container.Resolve<INodeTransformer>();

            var testData =
                new ManyChildrenNode("child1");

            var result = transformer.Transform(testData);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NoChildrenNode));
        }
    }
}

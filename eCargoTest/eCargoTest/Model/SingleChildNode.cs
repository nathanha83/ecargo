﻿using System;

namespace eCargoTest.Model
{
    public class SingleChildNode : Node
    {
        public Node Child { get; private set; }

        public SingleChildNode(string name, Node child) : base(name)
        {
            Child = child;
        }

        public override string SubDescribe(int indent)
        {
            if (Child == null) return string.Empty;

            return Child.Describe(indent);
        }

        public override Node SelfTransform()
        {
            //no child
            if(Child == null)
            {
                return new NoChildrenNode(Name);
            }

            Child = Child.SelfTransform();

            return this;
        }

    }
}

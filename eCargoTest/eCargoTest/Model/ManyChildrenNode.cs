﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace eCargoTest.Model
{
    public class ManyChildrenNode : Node
    {
        public IEnumerable<Node> Children { get; private set; }

        public ManyChildrenNode(string name, params Node[] children) : base(name)
        {
            Children = children;
        }

        public override string SubDescribe(int indent)
        {
            if (Children == null || !Children.Any()) return string.Empty;

            List<string> list = new List<string>();

            foreach (var child in Children)
            {
                list.Add(child.Describe(indent));
            }

            return string.Join("," + Environment.NewLine, list);
        }


        public override Node SelfTransform()
        {
            if (Children == null || !Children.Any()) return new NoChildrenNode(Name);

            //we have to go through all the enumarable anyway
            List<Node> children = new List<Node>();

            foreach (var node in Children)
            {
                children.Add(node.SelfTransform());
            }

            switch(children.Count)
            {
                case 1:
                    return new SingleChildNode(Name, children[0]);
                case 2:
                    return new TwoChildrenNode(Name, children[0], children[1]);
                default:
                    Children = children;
                    return this;
            }
        }
    }
}

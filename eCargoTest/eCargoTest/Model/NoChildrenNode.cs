﻿using eCargoTest.Common;
using System;

namespace eCargoTest.Model
{
    public class NoChildrenNode : Node
    {
        public NoChildrenNode(string name) : base(name)
        {
        }

        public override Node SelfTransform()
        {
            return this;
        }

        public override string SubDescribe(int indent)
        {
            return string.Empty;
        }
    }
}

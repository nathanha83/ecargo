﻿using eCargoTest.Common;
using eCargoTest.Interface;
using System;
using System.Text;

namespace eCargoTest.Model
{
    public abstract class Node : ISelfDescribable
    {
        public string Name { get; }
        protected Node(string name)
        {
            Name = name;
        }

        #region Describe support Functions

        public virtual string Describe(int indent)
        {
            StringBuilder result = new StringBuilder();
            result.Append(Constants.Quote);

            for (int i = 0; i < indent; i++)
            {
                result.Append(Constants.Indent);
            }

            //let sub types handle their own cases
            string childDescribe = SubDescribe(indent + 1);

            if (!string.IsNullOrWhiteSpace(childDescribe))
                result.AppendFormat("new {0}(\"{1}\",{2}{3})", GetType().Name, Name, Environment.NewLine, childDescribe);
            else
                result.AppendFormat("new {0}(\"{1}\")", GetType().Name, Name);

            return result.ToString();
        }

        public abstract string SubDescribe(int indent);

        #endregion

        #region Transform Support Functions

        public abstract Node SelfTransform();

        #endregion
    }
}

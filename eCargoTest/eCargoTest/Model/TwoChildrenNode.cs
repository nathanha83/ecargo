﻿using System.Collections.Generic;
using System;

namespace eCargoTest.Model
{
    public class TwoChildrenNode : Node
    {
        public Node FirstChild { get; private set; }

        public Node SecondChild { get; private set; }

        public TwoChildrenNode(string name, Node first, Node second) : base(name)
        {
            FirstChild = first;
            SecondChild = second;
        }

        public override string SubDescribe(int indent)
        {
            List<string> list = new List<string>();

            if (FirstChild != null)
                list.Add(FirstChild.Describe(indent));
            if (SecondChild != null)
                list.Add(SecondChild.Describe(indent));

            return string.Join("," + Environment.NewLine, list);
        }

        public override Node SelfTransform()
        {
            // no child
            if (FirstChild == null && SecondChild == null)
            {
                return new NoChildrenNode(Name);
            }

            // single child
            if(FirstChild == null || SecondChild == null)
            {
                return new SingleChildNode(Name, FirstChild != null? FirstChild.SelfTransform(): SecondChild.SelfTransform());
            }

            //ok both children exist
            FirstChild = FirstChild.SelfTransform();
            SecondChild = SecondChild.SelfTransform();

            return this;
        }

    }
}

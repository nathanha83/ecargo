﻿using eCargoTest.Model;
using System.Threading.Tasks;

namespace eCargoTest.Interface
{
    public interface INodeWriter
    {
        Task WriteToFileAsync(Node node, string filePath);
    }
}

﻿using eCargoTest.Model;

namespace eCargoTest.Interface
{
    public interface INodeDescriber
    {
        string Describe(Node node);
    }
}

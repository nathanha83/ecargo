﻿namespace eCargoTest.Interface
{
    interface ISelfDescribable
    {
        string Describe(int indent);
    }
}

﻿using eCargoTest.Interface;
using System.Text;
using eCargoTest.Model;

namespace eCargoTest.Impl
{
    public class NodeDescriber : INodeDescriber
    {
        public string Describe(Node node)
        {
            //StringBuilder result = new StringBuilder();
            //result.Append(@"// result is:");
            //result.AppendFormat("{0}{1}", System.Environment.NewLine, node.Describe(0));

            // return result.ToString();

            return node.Describe(0);
        }
    }
}

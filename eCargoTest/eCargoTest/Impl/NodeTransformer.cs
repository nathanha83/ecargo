﻿using eCargoTest.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCargoTest.Model;

namespace eCargoTest.Impl
{
    public class NodeTransformer : INodeTransformer
    {
        public Node Transform(Node node)
        {
            Node newNode = node.SelfTransform();

            return newNode;
        }
    }
}

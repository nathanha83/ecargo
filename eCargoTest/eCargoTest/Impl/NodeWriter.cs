﻿using eCargoTest.Interface;
using System.Threading.Tasks;
using eCargoTest.Model;
using System.IO;
using Microsoft.Practices.Unity;

namespace eCargoTest.Impl
{
    public class NodeWriter : INodeWriter
    {
        [Dependency]
        public INodeDescriber Describer { get; set; }

        public Task WriteToFileAsync(Node node, string filePath)
        {
            return Task.Run(
                () =>
                {
                    if (Describer != null)
                    {
                        var dir = Path.GetDirectoryName(filePath);
                        if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                        File.WriteAllText(filePath, Describer.Describe(node));
                    }
                }
                );
        }
    }
}

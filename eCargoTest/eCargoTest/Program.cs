﻿using eCargoTest.Impl;
using eCargoTest.Interface;
using eCargoTest.Model;
using Microsoft.Practices.Unity;
using System;
using System.IO;

namespace eCargoTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType(typeof(INodeDescriber), typeof(NodeDescriber));
            container.RegisterType(typeof(INodeWriter), typeof(NodeWriter));

            TestWrite(container);

            Console.In.ReadLine();

        }

        static void TestDescribe()
        {
            var testData = new SingleChildNode("root",
                new TwoChildrenNode("child1",
                new NoChildrenNode("leaf1"),
                new SingleChildNode("child2",
                new NoChildrenNode("leaf2"))));
            INodeDescriber imp = new NodeDescriber();

            string output = imp.Describe(testData);

            Console.Out.WriteLine(output);
            Console.In.ReadLine();
        }

        static void TestTransform()
        {
            INodeTransformer implementation = new NodeTransformer();
            var testData = new ManyChildrenNode("root",
                new ManyChildrenNode("child1",
                    new ManyChildrenNode("leaf1"),
                    new ManyChildrenNode("child2",
                        new ManyChildrenNode("leaf2"))));
            var result = implementation.Transform(testData);

            INodeDescriber imp = new NodeDescriber();
            string output = imp.Describe(result);

            Console.Out.WriteLine(output);
            Console.In.ReadLine();
        }

        static async void TestWrite(IUnityContainer container)
        {
            var writer = container.Resolve<INodeWriter>();
            string filePath = @"C:\eCargoTestResult\node.txt";

            var testData = new NoChildrenNode("root");
            await writer.WriteToFileAsync(testData, filePath);
            var result = File.ReadAllText(filePath);
        }
    }
}
